# nginx-maintenance-message

Project to show a maintenance message when needed.

## Deploy it

oc new-app centos/nginx-112-centos7~https://gitlab.cern.ch/webservices/discourse-cern-tools/nginx-maintenance-message.git

## Configure it

Choose the route you want to point at, and change the service to point to `nginx-maintenance-message`.

## Considerations

When moving a service to point to `nginx-maintenance-message` service, send the following signal to nginx container in Discourse app:

```bash
# Gracefully terminate nginx
nginx -s quit
```

Once finish, and you switch back your route to point to the proper service, scale down `nginx-maintenance-message` deployment.

## Remove it

```bash
oc delete svc/nginx-maintenance-message
oc delete dc/nginx-maintenance-message
oc delete bc/nginx-maintenance-message

oc get is
oc delete is/nginx-maintenance-message
oc delete is/nginx-112-centos7
```
